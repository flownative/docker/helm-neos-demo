# Neos Demo Helm Chart

Helm chart for a Neos Demo site, based on the Beach Docker images.

This chart should not be used in production without further modification. Its purpose
is to demonstrate how to install a Neos Demo site using Helm, based on the Flownative Beach
Docker images located on Docker Hub.

The following instructions are based on a Docker for Mac setup, but you will likely be able to
follow the steps on other platforms, too. Note that these steps will not lead to a production-safe
setup, in particular Helm should be installed with TLS and strong certificates, as mentioned in
the Helm manual.

## Prerequisites 

Apart of a Kubernetes cluster, you also need a database server in order to run this Neos Demo site.
You can either install MySQL / MariaDB in the cluster (for example by using Helm) or use a
database server running on your host machine. The example values are configured to use a
MariaDB server running on your host at port 3307 with user "root" and password "password".

Before following the installation steps, make sure that you have a database server running and
an empty database called "beach-neos-demo".

## Installation

If you didn't do this already, first install Docker for Mac and enable Kubernetes using the
Docker for Mac control panel. We also assume that you have `kubectl` and `helm` installed. 

Make sure that `kubectl` is using your local Kubernetes cluster as its default context:

```bash
✗ kubectl config use-context docker-for-desktop
Switched to context "docker-for-desktop".
```

Install Tiller and make sure that its running:
```bash
✗ helm init
$HELM_HOME has been configured at /Users/yourusername/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation

✗ kubectl get pods -n kube-system
NAME                                     READY   STATUS    RESTARTS   AGE
coredns-584795fc57-l98q5                 1/1     Running   0          4h7m
coredns-584795fc57-m24kj                 1/1     Running   0          4h7m
etcd-docker-desktop                      1/1     Running   0          4h6m
kube-apiserver-docker-desktop            1/1     Running   0          4h6m
kube-controller-manager-docker-desktop   1/1     Running   0          4h6m
kube-proxy-b59rv                         1/1     Running   0          4h7m
kube-scheduler-docker-desktop            1/1     Running   0          4h6m
tiller-deploy-5c5b6f6567-wcsqj           1/1     Running   0          71s
```

Install the Helm chart using example values for a local setup:

```bash
✗ helm install --name neos5 -f examples/values-for-local-kubernetes.yaml .
NAME:   neos5
LAST DEPLOYED: Wed Sep 25 12:16:40 2019
NAMESPACE: default
STATUS: DEPLOYED

RESOURCES:
==> v1/Deployment
NAME                   READY  UP-TO-DATE  AVAILABLE  AGE
neos5-beach-neos-demo  0/1    1           0          0s

==> v1/Pod(related)
NAME                                   READY  STATUS    RESTARTS  AGE
neos5-beach-neos-demo-79c496978-jwm2j  0/2    Init:0/1  0         0s

==> v1/Service
NAME                   TYPE      CLUSTER-IP  EXTERNAL-IP  PORT(S)       AGE
neos5-beach-neos-demo  NodePort  10.97.72.1  <none>       80:30038/TCP  0s
```

When the containers are ready, and your database setup uses the default user, password and
database name (see Prerequisites), you should be able to access the demo site from your
computer using the service port number mentioned in the `helm install` output:

```yaml
✗ curl http://localhost:30038
<!DOCTYPE html><html lang="en-US"><!--
	This website is powered by Neos, the Open Source Content Application Platform licensed under the GNU/GPL.
	Neos is based on Flow, a powerful PHP application framework licensed under the MIT license.

	More information and contribution opportunities at https://www.neos.io
-->
…
``` 

## Cleaning Up

Uninstall the Helm chart with:

```bash
✗ helm delete --purge neos5
release "neos5" deleted
```
